# Mines (with Treasurer support)

* Short name: `tsm_mines`
* Version: 0.6

## Using the mod
This mod adds abandoned mines in the underground.

You can find chests with different stuff like food, resources, ingots or even tools.
If you have the Treasurer mod enabled and some treasure registration mods (TRMs), you
will find stuff from other mods as well.

Remember that this mod is still in alpha stage!

## Settings
You can change the spawing of mines by adding/changing these values in `minetest.conf`
or advanced configuration:

* `mines_deep_min`: At this depth mines are created (default: `-64`)
* `mines_deep_max`: Up to this depth mines are created (default: `-380`)
* `mines_spawnfactor`: Increase this value to generate more mines (default: `1.5`)

## License
This mod is free software and licensed under the MIT License.

## Contributors
* Started by BlockMen in 2013
* Forked by Wuzzy later
* cHyper (For setting mine depth by configuration)
